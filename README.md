Reference for Figma Layout on React.js

Link to layouts: https://www.figma.com/file/bx2eKDjhiODiW4KR3XiyZlfl/Oculisa

Technology stack: react + redux + saga.

Layouts should be composed in adaptive (layouts for mobile and tablet versions are also present by the link above).

Everything that can and is logical to use with redux should be used with it.

The architecture and code should be convenient for support and reading.

There must be a separate reducer for each element and, if necessary, saga (empty, but redux-actions must be created in order for it to be convenient to tie it up with back-end in the future).

Also, in the payment page you need to integrate payment through the fondy.eu widget.

Main page: there is only one feature here - the hover effect when hovering over one particular character on the first screen. Animation is the same as here: https://onix.design/?ref=lapaninja

Product page: the mouse wheel should spin up and down, indicating that you need to scroll down the page.

Prices: changing currency, prices in other currencies will provide.

The input field style: https://ui-kit.chulakov.ru/ such as here:

Animation menu for mobile and tablet: the same as here https://ficto.app