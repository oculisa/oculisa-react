import * as types from './types';

import axios from 'axios';

import Amplify, { Auth } from 'aws-amplify';

let authInterceptor;

export const auth = token => {
  authInterceptor = axios.interceptors.request.use(function(config) {
    config.headers.Authorization = `Bearer ${token}`;

    return config;
  });
  return {
    type: types.AUTH,
    payload: {
      token: token,
    },
  };
};

export const logOut = () => {
  axios.interceptors.request.eject(authInterceptor);
  Auth.signOut(); // async maybe
  localStorage.removeItem('token');

  return {
    type: types.LOG_OUT,
  };
};
