import React from 'react';
import { Link } from 'react-router-dom';
import OculisaSwitchButton from '../Common/OculisaSwicthButton';
import classNames from 'classnames';
import { useTranslation, initReactI18next } from 'react-i18next';

function SiteCardItem({ isChecked, onEdit, site, onCheck }) {
  const { t } = useTranslation();
  return (
    <div className="col-lg-4 col-md-6 col-sm-12">
      <div className="sites ">
        <div className="top flex-container">
          <p className="title">{site}</p>
          <OculisaSwitchButton isChecked={isChecked} onCheck={onCheck} />
        </div>
        <div className="actions">
          <Link
            to={`/leads/${site}`}
            className={classNames('btn action-pills', {
              active: isChecked,
              'not-active': !isChecked,
            })}
          >
            {t('dashboardManage')}
          </Link>
          <button
            onClick={onEdit}
            className={classNames('action-pills', {
              active: isChecked,
              'not-active': !isChecked,
            })}
          >
            {t('dashboardEdit')}
          </button>
        </div>
      </div>
    </div>
  );
}
export default SiteCardItem;
