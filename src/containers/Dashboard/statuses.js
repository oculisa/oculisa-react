export const STATUSES = {
  SALE: 'sale',
  NO_SALE: 'no sale',
  ACTIVE: 'active',
  PENDING: 'pending',
};
