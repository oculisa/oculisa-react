import React, { useContext, useEffect } from 'react';
import { ModalContext } from 'context/modal';
import { withRouter } from 'react-router';

function PayModal({ location }) {
  const { search } = location;

  const arr = search.replace('?', '').split('&');
  console.log(arr);

  const leads = arr[0].split('=')[1];
  const currency = arr[1].split('=')[1];
  const amount = +arr[2].split('=')[1];

  useEffect(() => {
    const script = document.createElement('script');
    script.async = true;
    script.src = 'https://pay.fondy.eu/latest/checkout-vue/checkout.js';
    script.onload = function () {
      const { host, protocol } = window.location;

      var Options = {
        options: {
          methods: ['card', 'wallets', 'local_methods'],
          methods_disabled: [],
          card_icons: ['mastercard', 'visa'],
          active_tab: 'card',
          default_country: 'UA',
          countries: ['PL', 'IE', 'GB', 'CZ', 'GE', 'RU'],
          fields: false,
          // title: 'title',
          link: 'https://oculisa.com',
          full_screen: true,
          button: true,
          locales: [
            'cs',
            'de',
            'en',
            'es',
            'fr',
            'hu',
            'it',
            'ko',
            'lv',
            'pl',
            'ro',
            'ru',
            'sk',
            'uk',
          ],
          email: true,
        },
        params: {
          merchant_id: 1396424,
          required_rectoken: 'y',
          currency: currency,
          amount: amount * 100,
          order_desc: `${leads} leads`,
          response_url: `${protocol}//${host}/payment-and-balance?sucess`,
        },
        messages: {
          pl: {
            card_number: 'Numer karty',
            my_title: 'Cel płatności',
            my_order_desc: 'Płatność testowa',
          },
          en: {
            card_number: 'Card number',
            my_title: 'Order description',
            my_order_desc: 'Test order',
          },
        },
      };

      window.fondy('#fondy-pay', Options);
    };

    const link = document.createElement('link');

    link.href = 'https://pay.fondy.eu/latest/checkout-vue/checkout.css';
    link.rel = 'stylesheet';

    document.body.append(script);

    document.body.append(link);
  }, [amount, currency, leads]);

  return <div id="fondy-pay"></div>;
}

export default withRouter(PayModal);
