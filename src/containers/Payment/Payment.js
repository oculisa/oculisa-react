import { ModalContext } from 'context/modal';
import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { withTranslation } from 'react-i18next';

import { Link, withRouter } from 'react-router-dom';

const masterCardLogo = require('../../assets/images/master-card-logo.svg');
const verifiedByVisaLogo = require('../../assets/images/verified-by-visa.svg');

class Payment extends Component {
  state = {
    leadsCount: 200,
  };

  changeLeads = (inc) => {
    let { leadsCount } = this.state;

    let value = (leadsCount += inc ? 100 : -100);

    if (value < 0) {
      value = 0;
    }

    this.setState({ leadsCount: value });
  };

  componentDidMount() {}

  pay = () => {
    // this.context('pay');
    this.props.history.push(
      `pay?leadsCount=${this.state.leadsCount}&currency=USD&price=${this.state.leadsCount * 1.59}`
    );
  };
  render() {
    const { leadsCount } = this.state;

    return (
      <div className="container payments">
        <Row>
          <Col lg="4" md="6" sm="12">
            <div className="current-balance">
              <p className="title">{this.props.t('paymentCurrentBalance')}</p>
              <p className="leads-number">100 {this.props.t('leads')}</p>
              <div className="line-button-container">
                <Link to="/pricing" className="line-button">
                  {this.props.t('paymentPricingInform')}
                </Link>
              </div>
              <span className="icon-container clickable">
                <span className="icon-reload-icon" />
              </span>
            </div>
            <div className="cards-logo-container">
              <img className="cards-logo" src={masterCardLogo} alt="Master card logo" />
              <img className="cards-logo ml-10" src={verifiedByVisaLogo} alt="Visa card logo" />
            </div>
          </Col>
          <Col lg="8" md="6" sm="12">
            <div className="current-balance">
              <p className="title">{this.props.t('paymnetPurchaseLeads')}</p>
              <p className="request-number-label">{this.props.t('paymentSelectText')}</p>
              <div className="payment-amount">
                <div className="number">
                  {/* // TODO: change to buttons and input */}
                  <span className="controls clickable" onClick={() => this.changeLeads(false)}>
                    -
                  </span>
                  <span className="request-number">{leadsCount}</span>
                  <span className="controls clickable" onClick={() => this.changeLeads(true)}>
                    +
                  </span>
                </div>
                <div className="amount">
                  <p className="label">{this.props.t('paymentAmount')}</p>
                  <p className="total">
                    {/* {this.props.t('currency')} */}
                    {leadsCount * 1.59}$
                  </p>
                </div>
              </div>
              <div className="tariff">
                <span className="label">{this.props.t('paymentTarrif')}</span>
                <span className="tariff-type">{this.props.t('priceGrowth')}</span>
                <span className="percent">-25%</span>
              </div>
              <div className="submit-section">
                <button className="purple-button btn" onClick={this.pay}>
                  {this.props.t('paymentComplete')}
                </button>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

Payment.contextType = ModalContext;

export default withRouter(withTranslation()(Payment));
